﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bateria.BO.DTO
{
    internal class Win32_PortableBattery : Win32_Battery
    {
        /// <summary>
        /// Time required to fully charge the battery.
        /// </summary>
        public int BatteryRechargeTime { get; set; }
        /// <summary>
        /// Multiplication factor of the DesignCapacity value to ensure that the milliwatt hour value does not overflow for Smart Battery Data Specification (SBDS) implementations.
        /// </summary>
        public int CapacityMultiplier { get; set; }
    }
}
