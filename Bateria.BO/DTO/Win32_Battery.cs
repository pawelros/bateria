﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bateria.BO.DTO
{
    /// <summary>
    /// Win32_Battery class
    /// http://msdn.microsoft.com/en-us/library/aa394074.aspx
    /// </summary>
    internal class Win32_Battery
    {
        /// <summary>
        /// Availability and status of the device.
        /// </summary>
        public AvailabilityType Availability { get; set; }
        /// <summary>
        /// Status of the battery. The value 10 (Undefined) is not valid in the CIM schema because in DMI it represents that no battery is installed. In this case, the object should not be instantiated.
        /// </summary>
        public BatteryStatusType BatteryStatus { get; set; }
        /// <summary>
        /// Short description of the object—a one-line string.
        /// </summary>
        public string Caption { get; set; }
        /// <summary>
        /// Enumeration that describes the battery's chemistry.
        /// </summary>
        public ChemistryType Chemistry { get; set; }
        /// <summary>
        /// Name of the first concrete class that appears in the inheritance chain used in the creation of an instance. When used with the other key properties of the class, the property allows all instances of this class and its subclasses to be identified uniquely. 
        /// </summary>
        public string CreationClassName { get; set; }
        /// <summary>
        /// Description of the object.
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Design capacity of the battery in milliwatt-hours. If the property is not supported, enter 0 (zero).
        /// </summary>
        public int DesignCapacity { get; set; }
        /// <summary>
        /// Design voltage of the battery in millivolts. If the attribute is not supported, enter 0 (zero).
        /// </summary>
        public int DesignVoltage { get; set; }
        /// <summary>
        /// Identifies the battery.
        /// </summary>
        public string DeviceID { get; set; }
        /// <summary>
        /// Estimate of the percentage of full charge remaining.
        /// </summary>
        public int EstimatedChargeRemaining { get; set; }
        /// <summary>
        /// Estimate in minutes of the time to battery charge depletion under the present load conditions if the utility power is off, or lost and remains off, or a laptop is disconnected from a power source.
        /// </summary>
        public int EstimatedRunTime { get; set; }
        /// <summary>
        /// Battery's expected lifetime in minutes, assuming that the battery is fully charged. The property represents the total expected life of the battery, not its current remaining life, which is indicated by the EstimatedRunTime property.
        /// </summary>
        public int ExpectedLife { get; set; }
        /// <summary>
        /// Full charge capacity of the battery in milliwatt-hours. Comparison of the value to the DesignCapacity property determines when the battery requires replacement. A battery's end of life is typically when the FullChargeCapacity property falls below 80% of the DesignCapacity property. If the property is not supported, enter 0 (zero).
        /// </summary>
        public int FullChargeCapacity { get; set; }
        /// <summary>
        /// Date and time the object was installed. This property does not need a value to indicate that the object is installed.
        /// </summary>
        public DateTime InstallDate { get; set; }
        /// <summary>
        /// Maximum time, in minutes, to fully charge the battery. The property represents the time to recharge a fully depleted battery, not the current remaining charge time, which is indicated in the TimeToFullCharge property.
        /// </summary>
        public int MaxRechargeTime { get; set; }
        /// <summary>
        /// Defines the label by which the object is known.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Data Specification version number supported by the battery. If the battery does not support this function, the value should be left blank.
        /// </summary>
        public string SmartBatteryVersion { get; set; }

       /// <summary>
        /// Current status of the object. Various operational and nonoperational statuses can be defined. Operational statuses include: "OK", "Degraded", and "Pred Fail" (an element, such as a SMART-enabled hard disk drive, may be functioning properly but predicting a failure in the near future). Nonoperational statuses include: "Error", "Starting", "Stopping", and "Service". The latter, "Service", could apply during mirror-resilvering of a disk, reload of a user permissions list, or other administrative work. Not all such work is online, yet the managed element is neither "OK" nor in one of the other states.
       /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// Elapsed time in seconds since the computer system's UPS last switched to battery power, or the time since the system or UPS was last restarted, whichever is less. If the battery is "on line", 0 (zero) is returned.
        /// </summary>
        public int TimeOnBattery { get; set; }
        /// <summary>
        /// Remaining time to charge the battery fully in minutes at the current charging rate and usage.
        /// </summary>
        public int TimeToFullCharge { get; set; }

        #region Enums

        public enum AvailabilityType
        {
            /// <summary>Other</summary>
            Other = 1,
            /// <summary>Unknown</summary>
            Unknown,
            /// <summary>Running or Full Power</summary>
            RunningOrFullPower,
            /// <summary>Warning</summary>
            Warning,
            /// <summary>In Test</summary>
            InTest,
            /// <summary>Not Applicable</summary>
            NotApplicable,
            /// <summary>Power Off</summary>
            PowerOff,
            /// <summary>Off Line</summary>
            OffLine,
            /// <summary>Off Duty</summary>
            OffDuty,
            /// <summary>Degraded</summary>
            Degraded,
            /// <summary>Not Installed</summary>
            NotInstalled,
            /// <summary>Install Error</summary>
            InstallError,
            /// <summary>Power Save - Unknown. The device is known to be in a power save mode, but its exact status is unknown.</summary>
            PowerSaveUnknown,
            /// <summary>Power Save - Low Power Mode. The device is in a power save state but still functioning, and may exhibit degraded performance.</summary>
            PowerSaveLowPowerMode,
            /// <summary>Power Save - Standby. The device is not functioning, but could be brought to full power quickly.</summary>
            PowerSaveStandby,
            /// <summary>Power Cycle</summary>
            PowerCycle,
            /// <summary>Power Save - Warning. The device is in a warning state, though also in a power save mode.</summary>
            PowerSaveWarning
        }

        public enum BatteryStatusType
        {
            /// <summary>The battery is discharging.</summary>
            Discharging = 1,
            /// <summary>The system has access to AC so no battery is being discharged. However, the battery is not necessarily charging.</summary>
            AC,
            /// <summary>Fully Charged</summary>
            FullyCharged,
            /// <summary>Low</summary>
            Low,
            /// <summary>Critical</summary>
            Critical,
            /// <summary>Charging</summary>
            Charging,
            /// <summary>Charging and High</summary>
            ChargingAndHigh,
            /// <summary>Charging and Low</summary>
            ChargingAndLow,
            /// <summary>Charging and Critical</summary>
            ChargingAndCritical,
            /// <summary>Undefined</summary>
            Undefined,
            /// <summary>Partially Charged</summary>
            PartiallyCharged
        }

        public enum ChemistryType
        {
            /// <summary>Other</summary>
            Other = 1,
            /// <summary>Unknown</summary>
            Unknown,
            /// <summary>Lead Acid</summary>
            LeadAcid,
            /// <summary>Nickel Cadmium</summary>
            NickelCadmium,
            /// <summary>Nickel Metal Hydride</summary>
            NickelMetalHydride,
            /// <summary>Lithium-ion</summary>
            LithiumIon,
            /// <summary>Zinc air</summary>
            ZincAir,
            /// <summary>Lithium Polymer</summary>
            LithiumPolymer,
        }

        //public enum StatusType
        //{
        //    OK,
        //    Error,
        //    Degraded,
        //    Unknown,
        //    Starting,
        //    Stopping,
        //    Service,
        //    Stressed,
        //    NonRecover,
        //    NoContact,
        //    LostComm
        //}
    }
        #endregion
}
