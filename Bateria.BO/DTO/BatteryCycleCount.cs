﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bateria.BO.DTO
{
    internal class BatteryCycleCount
    {
        public bool Active { get; set; }
        public string Caption { get; set; }
        public int CycleCount { get; set; }
        public string Description { get; set; }
        public int Frequency_Object { get; set; }
        public int Frequency_PerfTime { get; set; }
        public int Frequeny_Sys100NS { get; set; }
        public string InstanceName { get; set; }
        public string Name { get; set; }
        public int Tag { get; set; }
        public int Timestamp_Object { get; set; }
        public int Timestamp_PerfTime { get; set; }
        public int Timestamp_Sys100NS { get; set; }
    }
}
