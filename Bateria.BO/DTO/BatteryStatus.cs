﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bateria.BO.DTO
{
    internal class BatteryStatus
    {
        public bool Active { get; set; }
        public int ChargeRate { get; set; }
        public bool Charging { get; set; }
        public bool Critical { get; set; }
        public int DischargeRate { get; set; }
        public bool Discharging { get; set; }
        public string InstanceName { get; set; }
        public bool PowerOnline { get; set; }
        public int RemainingCapacity { get; set; }
        public int Tag { get; set; }
        public int Voltage { get; set; }
    }
}
