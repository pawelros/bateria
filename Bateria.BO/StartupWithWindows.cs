﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Bateria.BO
{
    internal static  class StartupWithWindows
    {
        private const string registryPath = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run";
        private static string applicationName = System.Reflection.Assembly.GetExecutingAssembly().FullName;

        public static bool IsEnabled
        {
            get
            {
                RegistryKey registryKey = Registry.CurrentUser.OpenSubKey
                                    (registryPath, false);
                registryKey.GetValue(applicationName);
                int x;
                return true;
            }
        }

        public static void Enable()
        {
            RegistryKey registryKey = Registry.CurrentUser.OpenSubKey
                    (registryPath, true);
            registryKey.SetValue(applicationName, Assembly.GetEntryAssembly().Location);
        }
        public static void Disable()
        {
            RegistryKey registryKey = Registry.CurrentUser.OpenSubKey
                    (registryPath, true);
            registryKey.DeleteValue(applicationName);
        }
    }
}
