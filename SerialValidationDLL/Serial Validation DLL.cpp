/*
* Copyright 2013 Pawe� Rosi�ski All rights reserved.
*
* mailto:  rosiu@outlook.com
*
*/

// Serial Validation DLL.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "PseudoBase32Converter.h"

#define PIDKEY_LENGTH 100
#define DIGIT(a)  ((a) - '0')

BOOL APIENTRY DllMain(HANDLE /*hModule*/, DWORD  /*ul_reason_for_call*/,
					  LPVOID /*lpReserved*/)
{
	return TRUE;
}

#ifndef _DEBUG
BOOL WINAPI __DllMainCRTStartup(HINSTANCE hModule, DWORD ul_reason_for_call,
								LPVOID lpReserved)
{
	return DllMain(hModule, ul_reason_for_call, lpReserved);
}
#endif

UINT __stdcall Validate(MSIHANDLE hInstall)
{
	TCHAR key[PIDKEY_LENGTH];
	DWORD dwLen = sizeof(key) / sizeof(key[0]);

	///retrieve the text entered by the user
	UINT res = ::MsiGetProperty(hInstall, _T("PIDKEY"), key, &dwLen);
	if(res != ERROR_SUCCESS)
	{
		//fail the installation
		return 1;
	}

	TCHAR seed0 = key[0];
	TCHAR seed1 = key[1];
	TCHAR seed2 = key[2];
	TCHAR seed3 = key[3];
	
	TCHAR FirstSegment0 = PseudoBase32Converter::Transform(key[5], -3);
	TCHAR FirstSegment1 = PseudoBase32Converter::Transform(key[6], -5);
	TCHAR FirstSegment2 = PseudoBase32Converter::Transform(key[7], -7);
	TCHAR FirstSegment3 = PseudoBase32Converter::Transform(key[8], -11);

	TCHAR SecondSegment0 = PseudoBase32Converter::Transform(key[10], 2);
	TCHAR SecondSegment1 = PseudoBase32Converter::Transform(key[11], 4);
	TCHAR SecondSegment2 = PseudoBase32Converter::Transform(key[12], 7);
	TCHAR SecondSegment3 = PseudoBase32Converter::Transform(key[13], 17);

	TCHAR ThirdSegment0 = PseudoBase32Converter::Transform(key[15], -1 * (int)seed0);
	TCHAR ThirdSegment1 = PseudoBase32Converter::Transform(key[16], -1 * (int)seed1);
	TCHAR ThirdSegment2 = PseudoBase32Converter::Transform(key[17], -1 * (int)seed2);
	TCHAR ThirdSegment3 = PseudoBase32Converter::Transform(key[18], -1 * (int)seed3);

	bool snIsValid = (seed0 == FirstSegment0 && seed0 == SecondSegment0 && seed0 == ThirdSegment0
	&& seed1 == FirstSegment1 && seed1 == SecondSegment1 && seed1 == ThirdSegment1
	&& seed2 == FirstSegment2 && seed2 == SecondSegment2 && seed2 == ThirdSegment2
	&& seed3 == FirstSegment3 && seed3 == SecondSegment3 && seed3 == ThirdSegment3);

	TCHAR * serialValid = NULL;
	if(snIsValid)
		serialValid = _T("TRUE"); 
	else
	{
		//eventually say something to the user
		::MessageBox(0, _T("Invalid Serial Number"), _T("Message"), MB_ICONSTOP);

		serialValid = _T("FALSE");
	}

	res = ::MsiSetProperty(hInstall, _T("SERIAL_VALIDATION"), serialValid);
	if(res != ERROR_SUCCESS)
	{
		//fail the installation
		return 1;
	}

	//the validation succeeded - even the serial is wrong
	//if the SERIAL_VALIDATION was set to FALSE the installation will not continue
	return 0;
}

char Transform(char character, int transformation)
{

	const int Nomin = 48;
	const int Nomax = 57;

	const int AZmin = 65;
	const int AZmax = 90;


	int result = character;
	int i = transformation > 0 ? 1 : -1;
	int t = 0;

	while (t != transformation)
	{
		int sum = result + i;
		if (sum > Nomax && sum < AZmin && i==1)
		{
			result = AZmin;
		}
		else if(sum > Nomax && sum <AZmin && i==-1)
		{
			result = Nomax;
		}
		else if (sum > AZmax)
		{
			result = Nomin;
		}
		else if (sum < Nomin)
		{
			result = AZmax;
		}
		else
		{
			result += i;
		}

		t += i;
	}

	return (char)result;
}
