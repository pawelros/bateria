#include "stdafx.h"
#include "PseudoBase32Converter.h"

const int Nomin = 48;
const int Nomax = 57;

const int AZmin = 65;
const int AZmax = 90;

PseudoBase32Converter::PseudoBase32Converter(void)
{
}


PseudoBase32Converter::~PseudoBase32Converter(void)
{
}

char PseudoBase32Converter::Transform(char character, int transformation)
{
	int result = character;
	int i = transformation > 0 ? 1 : -1;
	int t = 0;

	while (t != transformation)
	{
		int sum = result + i;
		if (sum > Nomax && sum < AZmin && i==1)
		{
			result = AZmin;
		}
		else if(sum > Nomax && sum <AZmin && i==-1)
		{
			result = Nomax;
		}
		else if (sum > AZmax)
		{
			result = Nomin;
		}
		else if (sum < Nomin)
		{
			result = AZmax;
		}
		else
		{
			result += i;
		}

		t += i;
	}

	return (char)result;
}
