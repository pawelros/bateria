﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WindowsFormsApplication1;

namespace Keygen.UnitTests
{
    [TestClass]
    public class PseudoBase32ConverterTest
    {
        [TestMethod]
        public void Should_Transform_A_To_B_When_Plus_1()
        {
            char input = 'A';
            int transformation = 1;
            char expected = 'B';

            Assert.AreEqual(expected, PseudoBase32Converter.Transform(input, transformation));
        }

        [TestMethod]
        public void Should_Transform_A_To_Z_When_Plus_25()
        {
            char input = 'A';
            int transformation = 25;
            char expected = 'Z';

            Assert.AreEqual(expected, PseudoBase32Converter.Transform(input, transformation));
        }

        [TestMethod]
        public void Should_Transform_A_To_0_When_Plus_26()
        {
            char input = 'A';
            int transformation = 26;
            char expected = '0';

            Assert.AreEqual(expected, PseudoBase32Converter.Transform(input, transformation));
        }

        [TestMethod]
        public void Should_Transform_A_To_9_When_Plus_35()
        {
            char input = 'A';
            int transformation = 35;
            char expected = '9';

            Assert.AreEqual(expected, PseudoBase32Converter.Transform(input, transformation));
        }

        [TestMethod]
        public void Should_Transform_A_To_A_When_Plus_36()
        {
            char input = 'A';
            int transformation = 36;
            char expected = 'A';

            Assert.AreEqual(expected, PseudoBase32Converter.Transform(input, transformation));
        }

        [TestMethod]
        public void Should_Transform_A_To_A_When_Plus_72()
        {
            char input = 'A';
            int transformation = 36;
            char expected = 'A';

            Assert.AreEqual(expected, PseudoBase32Converter.Transform(input, transformation));
        }

        [TestMethod]
        public void Should_Transform_B_To_A_When_Minus_1()
        {
            char input = 'B';
            int transformation = -1;
            char expected = 'A';

            Assert.AreEqual(expected, PseudoBase32Converter.Transform(input, transformation));
        }

        [TestMethod]
        public void Should_Transform_A_To_9_When_Minus_1()
        {
            char input = 'A';
            int transformation = -1;
            char expected = '9';

            Assert.AreEqual(expected, PseudoBase32Converter.Transform(input, transformation));
        }

        [TestMethod]
        public void Should_Transform_9_To_0_When_Minus_9()
        {
            char input = '9';
            int transformation = -9;
            char expected = '0';

            Assert.AreEqual(expected, PseudoBase32Converter.Transform(input, transformation));
        }

        [TestMethod]
        public void Should_Transform_0_To_Z_When_Minus_1()
        {
            char input = '0';
            int transformation = -1;
            char expected = 'Z';

            Assert.AreEqual(expected, PseudoBase32Converter.Transform(input, transformation));
        }

        [TestMethod]
        public void Should_Transform_0_To_A_When_Minus_26()
        {
            char input = '0';
            int transformation = -26;
            char expected = 'A';

            Assert.AreEqual(expected, PseudoBase32Converter.Transform(input, transformation));
        }
    }
}
