﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class Generator
    {
        Random rd = new Random();

        public string GenerateKey()
        {
            string result = string.Empty;
            string seed = GenerateSeed();

            result += seed[0];
            result += seed[1];
            result += seed[2];
            result += seed[3];
            result += "-";

            result += PseudoBase32Converter.Transform(seed[0], 3);
            result += PseudoBase32Converter.Transform(seed[1], 5);
            result += PseudoBase32Converter.Transform(seed[2], 7);
            result += PseudoBase32Converter.Transform(seed[3], 11);
            result += "-";

            result += PseudoBase32Converter.Transform(seed[0], -2);
            result += PseudoBase32Converter.Transform(seed[1], -4);
            result += PseudoBase32Converter.Transform(seed[2], -7);
            result += PseudoBase32Converter.Transform(seed[3], -17);
            result += "-";

            result += PseudoBase32Converter.Transform(seed[0], (int)seed[0]);
            result += PseudoBase32Converter.Transform(seed[1], (int)seed[1]);
            result += PseudoBase32Converter.Transform(seed[2], (int)seed[2]);
            result += PseudoBase32Converter.Transform(seed[3], (int)seed[3]);

            return result;
        }

        private string GenerateSeed()
        {
            string result = string.Empty;
            for (int i = 0; i < 4; i++)
            {
                int number = (char)rd.Next(PseudoBase32Converter.Nomin, PseudoBase32Converter.Nomax);
                int alpha = (char)rd.Next(PseudoBase32Converter.AZmin, PseudoBase32Converter.AZmax);
                int numberOrAlpha = rd.Next(0, 100);

                if (numberOrAlpha > 50)
                {
                    result += number;
                }
                else
                {
                    result += alpha;
                }
            }
        
            return result;
        }
    }
}
