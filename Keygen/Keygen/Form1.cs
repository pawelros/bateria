﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        object lock_object = new object();

        private async void button1_Click(object sender, EventArgs e)
        {
            this.progressBar1.Visible = true;
            Stopwatch sw = new Stopwatch();
            sw.Start();
            if (!File.Exists(this.textBox1.Text))
            {
                File.Create(this.textBox1.Text);
            }
            else
            {
                File.WriteAllText(this.textBox1.Text, string.Empty);
            }
            int howMany = Int32.Parse(this.numericUpDown1.Value.ToString());
            do
            {
                await Task.Run(() => GenerateKeys());
            } while (File.ReadAllLines(this.textBox1.Text).Length < howMany);
            this.progressBar1.Visible = false;

            List<string> lines = new List<string>();

            foreach (var line in File.ReadAllLines(this.textBox1.Text))
            {
                if (lines.Count < howMany)
                {
                    lines.Add(line);
                }
            }

            File.WriteAllLines(this.textBox1.Text, lines);

            sw.Stop();

            Console.WriteLine(string.Format("Key generation took: {0}", sw.Elapsed));
        }

        async Task GenerateKeys()
        {
            Generator generator = new Generator();
            HashSet<string> sorted = new HashSet<string>();

            Parallel.For(0, Int32.Parse(this.numericUpDown1.Value.ToString()), i =>
            {
                string key = generator.GenerateKey();
                lock (lock_object)
                {
                    sorted.Add(key);
                }
            });

            foreach (string key in sorted)
            {
                File.AppendAllText(this.textBox1.Text, string.Concat(key, "\n"));
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            bool isValid = Validator.isKeyValid(this.textBox2.Text);

            if (isValid)
            {
                this.label1.Text = "VALID";
                this.label1.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                this.label1.Text = "INVALID";
                this.label1.ForeColor = System.Drawing.Color.Red;
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            List<string> invalidKeys = new List<string>();
            bool isValid = Validator.ValidateFileWithKeys(this.textBox3.Text, invalidKeys);

            if (isValid)
            {
                this.label2.Text = "VALID";
                this.label2.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                this.label2.Text = "INVALID";
                this.label2.ForeColor = System.Drawing.Color.Red;

                Invalid inv = new Invalid(invalidKeys);
                inv.ShowDialog();
            }
        }
    }
}
