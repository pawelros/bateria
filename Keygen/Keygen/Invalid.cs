﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Invalid : Form
    {
        public Invalid(List<string> InvalidKeys)
        {
            InitializeComponent();
            this.textBox1.Text += "Znaleziono nieprawidłowe klucze: \r\n";

            foreach (string key in InvalidKeys)
            {
                this.textBox1.Text += string.Concat(key, "\r\n");
            }
        }
    }
}
