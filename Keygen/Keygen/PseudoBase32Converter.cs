﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    internal class PseudoBase32Converter
    {
        public const int Nomin = 48;
        public const int Nomax = 57;

        public const int AZmin = 65;
        public const int AZmax = 90;

        public static char Transform(char character, int transformation)
        {
            int result = character;
            int i = transformation > 0 ? 1 : -1;
            int t = 0;

            while (t != transformation)
            {
                int sum = result + i;
                if (sum > Nomax && sum < AZmin && i==1)
                {
                    result = AZmin;
                }
                else if(sum > Nomax && sum <AZmin && i==-1)
                {
                    result = Nomax;
                }
                else if (sum > AZmax)
                {
                    result = Nomin;
                }
                else if (sum < Nomin)
                {
                    result = AZmax;
                }
                else
                {
                    result += i;
                }

                t += i;
            }

            return (char)result;
        }
    }
}
