﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class Validator
    {
        public static bool ValidateFileWithKeys(string filePath, List<string> invalidKeys)
        {
            bool result = true;
            var keys = File.ReadAllLines(filePath);

            foreach (var key in keys)
            {
                if (!isKeyValid(key))
                {
                    result = false;
                    invalidKeys.Add(key);
                }
            }
            return result;
        }

        public static bool isKeyValid(string key)
        {
            //0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
            //A C M U - C E O W - A  C  I  Q  -  D  F  P  X

            char seed0 = key[0];
            char seed1 = key[1];
            char seed2 = key[2];
            char seed3 = key[3];

            char FirstSegment0 = PseudoBase32Converter.Transform(key[5], -3);
            char FirstSegment1 = PseudoBase32Converter.Transform(key[6], -5);
            char FirstSegment2 = PseudoBase32Converter.Transform(key[7], -7);
            char FirstSegment3 = PseudoBase32Converter.Transform(key[8], -11);

            char SecondSegment0 = PseudoBase32Converter.Transform(key[10], 2);
            char SecondSegment1 = PseudoBase32Converter.Transform(key[11], 4);
            char SecondSegment2 = PseudoBase32Converter.Transform(key[12], 7);
            char SecondSegment3 = PseudoBase32Converter.Transform(key[13], 17);

            char ThirdSegment0 = PseudoBase32Converter.Transform(key[15], -1 * (int)seed0);
            char ThirdSegment1 = PseudoBase32Converter.Transform(key[16], -1 * (int)seed1);
            char ThirdSegment2 = PseudoBase32Converter.Transform(key[17], -1 * (int)seed2);
            char ThirdSegment3 = PseudoBase32Converter.Transform(key[18], -1 * (int)seed3);

            return (seed0 == FirstSegment0 && seed0 == SecondSegment0 && seed0 == ThirdSegment0
                && seed1 == FirstSegment1 && seed1 == SecondSegment1 && seed1 == ThirdSegment1
                && seed2 == FirstSegment2 && seed2 == SecondSegment2 && seed2 == ThirdSegment2
                && seed3 == FirstSegment3 && seed3 == SecondSegment3 && seed3 == ThirdSegment3);

        }
    }
}
